import filter.BasicContourExtractorFilter;
import filter.GreyLevelFilter;

public class Main {

	public Main() {
		try {

			//			IImageFilteringEngine im = new SingleThreadedImageFilteringEngine();
			//			im.loadImage("TP56/TEST_IMAGES/FourCircles.png");
			//			im.applyFilter(new GreyLevelFilter());
			//			im.applyFilter(new BasicContourExtractorFilter());
			//			im.writeOutPngImage("TP56/TEST_IMAGES/tmp.png");


			IImageFilteringEngine im = new MultiThreadedImageFilteringEngine(1);
			for (int i = 0; i < 10; i++) {
				long t = System.currentTimeMillis();
				im.loadImage("TP56/TEST_IMAGES/eae356a5998cf67ac2fb6cae536cacc4.jpg");
				im.applyFilter(new GreyLevelFilter());
				im.applyFilter(new BasicContourExtractorFilter());
				t = System.currentTimeMillis()-t;
				System.out.println(t);
				//System.out.println("nbWorkers = "+2+" "+t+"ms");
				//im.writeOutPngImage("TP56/TEST_IMAGES/tmp.png");
			}

			


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Main();
	}

}
