import java.awt.image.BufferedImage;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import filter.IFilter;

public class Worker extends Thread {

	private CyclicBarrier cy;
	private BufferedImage imgIn, imgOut;
	private int fromX, toX, fromY, toY;
	private IFilter filter;
	private boolean stop;

	public Worker(CyclicBarrier cy) {
		super();
		this.cy = cy;
	}

	@Override
	public void run() {
		try {
			//Optimisation
			while(true){
				//System.out.println("Worker :" + this.getName() + " is sleeping");
				cy.await();
				if(stop)
					return;
				//System.out.println("Worker :" + this.getName() + " is working");
				for (int x = fromX+filter.getMargin(); x < toX-filter.getMargin(); x++) {
					for (int y = fromY+filter.getMargin(); y < toY-filter.getMargin(); y++) {
						filter.applyFilterAtPoint(x, y, imgIn, imgOut);
					} // EndFor y
				} // EndFor x
				cy.await();
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			e.printStackTrace();
		}

	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	@Override
	public String toString() {
		StringBuilder stb = new StringBuilder();
		stb.append("Hello, I'm "+getName()+".\nI'm working between ");
		stb.append(fromX + "px ");
		stb.append(toX + "px ");
		stb.append(fromY + "px ");
		stb.append(toY + "px ");
		return stb.toString();
	}

	public BufferedImage getImgOut() {
		return imgOut;
	}

	public void setImgOut(BufferedImage imgOut) {
		this.imgOut = imgOut;
	}

	public IFilter getFilter() {
		return filter;
	}

	public void setFilter(IFilter filter) {
		this.filter = filter;
	}

	public BufferedImage getImgIn() {
		return imgIn;
	}

	public void setImgIn(BufferedImage img) {
		this.imgIn = img;
	}

	public int getFromX() {
		return fromX;
	}

	public void setFromX(int fromX) {
		this.fromX = fromX;
	}

	public int getToX() {
		return toX;
	}

	public void setToX(int toX) {
		this.toX = toX;
	}

	public int getFromY() {
		return fromY;
	}

	public void setFromY(int fromY) {
		this.fromY = fromY;
	}

	public int getToY() {
		return toY;
	}

	public void setToY(int toY) {
		this.toY = toY;
	}

}
