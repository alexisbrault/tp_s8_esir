import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;

import javax.imageio.ImageIO;

import filter.IFilter;

public class MultiThreadedImageFilteringEngine implements IImageFilteringEngine {

	private CyclicBarrier barrier;
	private List<Worker> threadList;

	private BufferedImage imgIn, imgOut;

	public MultiThreadedImageFilteringEngine(int nbWorkers) {
		barrier = new CyclicBarrier(nbWorkers+1);
		threadList = new ArrayList<Worker>();
		for(int i=0; i<nbWorkers;i++){
			Worker w = new Worker(barrier);
			w.start();
			threadList.add(w);
		}
	}

	@Override
	public void loadImage(String inputImage) throws Exception {
		setImg(ImageIO.read(new File(inputImage)));
	}

	@Override
	public void writeOutPngImage(String outFile) throws Exception {
		File f = new File(outFile);
		ImageIO.write(imgOut, "png", f);
		for(int i=0; i<threadList.size();i++){
			threadList.get(i).setStop(true);
		}
		barrier.await();
	}

	@Override
	public void setImg(BufferedImage newImg) {
		imgIn = newImg;
	}

	@Override
	public BufferedImage getImg() {
		for(int i=0; i<threadList.size();i++){
			threadList.get(i).setStop(true);
		}
		return imgOut;
	}

	@Override
	public void applyFilter(IFilter someFilter) {
		imgOut = new BufferedImage(imgIn.getWidth()-2*someFilter.getMargin(),
				imgIn.getHeight()-2*someFilter.getMargin(),
				BufferedImage.TYPE_INT_RGB);
		int size = threadList.size();
		//Opti verticale
		int r = imgIn.getHeight()/size;
		int prev_r = 0;
		int i;
		
		//System.out.println("Image("+imgIn.getWidth()+"px x"+imgIn.getHeight()+"px) splits into "+size+" parts of "+r+"px x "+imgIn.getHeight()+"px");
		for(i = 0;i<size;i++){
			Worker w = threadList.get(i);
			initWorker(someFilter, w);
			if(i>0)
				w.setFromY(prev_r-someFilter.getMargin());
			else
				w.setFromY(prev_r);
			prev_r+=r;
			if(i==(size-1))
				w.setToY(imgIn.getHeight());//Dans le cas ou les parts ne sont pas �gales
			else
				w.setToY(prev_r+someFilter.getMargin());

			//System.out.println(w);
		}		

		try {
			//System.out.println("Main :" + "Threads unlock");
			barrier.await();//Barrier qui lance l'exe
			//System.out.println("Main :" + "Execution launch");
			barrier.await();//Barrier qui attend l'exe
			//System.out.println("Main :" + "Execution ended");
			setImg(imgOut);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}

	private void initWorker(IFilter someFilter, Worker w) {
		w.setFilter(someFilter);
		w.setImgIn(imgIn);
		w.setImgOut(imgOut);
		w.setFromX(0);
		w.setToX(imgIn.getWidth());
	}

}
