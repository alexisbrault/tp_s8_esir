import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import filter.IFilter;

public class SingleThreadedImageFilteringEngine implements IImageFilteringEngine{

	private BufferedImage imgIn, imgOut;

	@Override
	public void loadImage(String inputImage) throws Exception {
		setImg(ImageIO.read(new File(inputImage)));
	}

	@Override
	public void writeOutPngImage(String outFile) throws Exception {
		File f = new File(outFile);
		ImageIO.write(imgOut, "png", f);

	}

	@Override
	public void setImg(BufferedImage newImg) {
		imgIn = newImg;
	}

	@Override
	public BufferedImage getImg() {
		return imgOut;
	}

	@Override
	public void applyFilter(IFilter someFilter) {
		imgOut = new BufferedImage(imgIn.getWidth()-2*someFilter.getMargin(),
				imgIn.getHeight()-2*someFilter.getMargin(),
				BufferedImage.TYPE_INT_RGB);
		for (int x = someFilter.getMargin(); x < imgIn.getWidth()-someFilter.getMargin(); x++) {
			for (int y = someFilter.getMargin(); y < imgIn.getHeight()-someFilter.getMargin(); y++) {
				someFilter.applyFilterAtPoint(x, y, imgIn, imgOut);
			} // EndFor y
		} // EndFor x
		this.setImg(imgOut);
	}

}
