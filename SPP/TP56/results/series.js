function loadSeries(){
var bar1 = new Tee.Bar();bar1.title = 'Image 800x450 GCF';bar1.data.values = [177, 99, 99, 109, 95, 104, 95];bar1.data.labels =  [1, 2, 4, 8, 16, 32, 64];Chart1.addSeries(bar1);
var bar2 = new Tee.Bar();bar2.title = 'Image 640x640 GCF';bar2.data.values = [97, 72, 110, 115, 113, 109, 126];bar2.data.labels =  [1, 2, 4, 8, 16, 32, 64];Chart1.addSeries(bar2);
var bar3 = new Tee.Bar();bar3.title = 'Image 800x450 GF';bar3.data.values = [31, 29, 43, 41, 42, 44, 42];bar3.data.labels =  [1, 2, 4, 8, 16, 32, 64];Chart1.addSeries(bar3);
var bar4 = new Tee.Bar();bar4.title = 'Image 640x640 GF';bar4.data.values = [32, 37, 52, 44, 58, 55, 52];bar4.data.labels =  [1, 2, 4, 8, 16, 32, 64];Chart1.addSeries(bar4);
var bar5 = new Tee.Bar();bar5.title = 'Image 4k GCF';bar5.data.values = [2098, 1935, 2535, 2499, 2527, 2351, 2411];bar5.data.labels =  [1, 2, 4, 8, 16, 32, 64];Chart1.addSeries(bar5);

}
