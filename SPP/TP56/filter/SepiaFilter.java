package filter;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class SepiaFilter implements IFilter {

	private final int DEPTH = 20;
	
	@Override
	public int getMargin() {
		return 0;
	}

	@Override
	public void applyFilterAtPoint(int x, int y, BufferedImage imgIn, BufferedImage imgOut) {
		int rgb    = imgIn.getRGB(x,y);
        // extracting red, green and blue components from rgb integer
        int red    = (rgb >> 16) & 0x000000FF;
        int green  = (rgb >>  8) & 0x000000FF;
        int blue   = (rgb      ) & 0x000000FF;

        int rr = red+(DEPTH*2);
        int gg = green+DEPTH;
        if(rr>255){
        	rr = 255;
        }
        if(gg>255){
        	gg = 255; 
        }
        
        int newRgb = (rr<<16) + (gg<<8) + blue;
        imgOut.setRGB(x, y, newRgb);
        
	}

}
