package filter;

import java.awt.image.BufferedImage;

public class GreyLevelFilter implements IFilter {

	@Override
	public int getMargin() {
		return 0;
	}

	@Override
	public void applyFilterAtPoint(int x, int y, BufferedImage imgIn, BufferedImage imgOut) {
		int rgb    = imgIn.getRGB(x,y);
        // extracting red, green and blue components from rgb integer
        int red    = (rgb >> 16) & 0x000000FF;
        int green  = (rgb >>  8) & 0x000000FF;
        int blue   = (rgb      ) & 0x000000FF;
        int avg = (red+green+blue)/3;
        int newRgb = (avg<<16) + (avg<<8) + avg;
        imgOut.setRGB(x, y, newRgb);
	}

}
