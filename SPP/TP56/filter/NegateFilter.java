package filter;

import java.awt.image.BufferedImage;

public class NegateFilter implements IFilter {

	@Override
	public int getMargin() {
		return 0;
	}

	@Override
	public void applyFilterAtPoint(int x, int y, BufferedImage imgIn, BufferedImage imgOut) {
		int rgb    = imgIn.getRGB(x,y);
        // extracting red, green and blue components from rgb integer
        int red    = (rgb >> 16) & 0x000000FF;
        int green  = (rgb >>  8) & 0x000000FF;
        int blue   = (rgb      ) & 0x000000FF;
        red = 255 - red;
        green = 255 - green;
        blue = 255 - blue;
        int newRgb = (red<<16) + (green<<8) + blue;
        imgOut.setRGB(x, y, newRgb);
	}

}
