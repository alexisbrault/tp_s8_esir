package filter;

import java.awt.image.BufferedImage;

public class BasicContourExtractorFilter implements IFilter {

	@Override
	public int getMargin() {
		return 1;
	}

	@Override
	public void applyFilterAtPoint(int x, int y, BufferedImage imgIn, BufferedImage imgOut) {
			
			double deltaX = (getBlue(imgIn.getRGB(x+1,y)) - getBlue(imgIn.getRGB(x-1,y)))/2.0;
			double deltaY = (getBlue(imgIn.getRGB(x,y+1)) - getBlue(imgIn.getRGB(x,y-1)))/2.0;
			double norm = Math.sqrt(deltaX*deltaX+deltaY*deltaY);
			int result = Math.max(0, 255 - (int)(8*norm));
			int newRgb =  (result<<16) | (result<<8) | result;
			imgOut.setRGB(x-getMargin(),y-getMargin(),newRgb);
		
	}

	private int getBlue(int rgb){
		return rgb & 0x000000FF;
	}

}
