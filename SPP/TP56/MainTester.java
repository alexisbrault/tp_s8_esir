

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import filter.BasicContourExtractorFilter;
import filter.GreyLevelFilter;

public class MainTester {

	private static BufferedImage base1;
	private static BufferedImage base1Grey;
	private static BufferedImage base1GreyContour;

	private static BufferedImage base2;
	private static BufferedImage base2Grey;
	private static BufferedImage base2GreyContour;

	private static BufferedImage base3, base4;
	
	private static BufferedImage red,green,blue,black,white,grey,white2;

	private int k = 64;
	
	private static int serieCounter;
	private static StringBuilder stb  = new StringBuilder();
	private static ArrayList<Integer> listData  = new ArrayList<Integer>();
	private static ArrayList<Integer> listLabels  = new ArrayList<Integer>();


	@BeforeClass
	public static void loadInMemoryImages() throws IOException{
		base1 = ImageIO.read(new File("TP56/TEST_IMAGES/15226222451_5fd668d81a_c.jpg"));
		base1Grey = ImageIO.read(new File("TP56/TEST_IMAGES/15226222451_5fd668d81a_c_Gray.png"));
		base1GreyContour = ImageIO.read(new File("TP56/TEST_IMAGES/15226222451_5fd668d81a_c_Gray_Contour.png"));

		base2 = ImageIO.read(new File("TP56/TEST_IMAGES/FourCircles.png"));
		base2Grey = ImageIO.read(new File("TP56/TEST_IMAGES/FourCircles_Gray.png"));
		base2GreyContour = ImageIO.read(new File("TP56/TEST_IMAGES/FourCircles_Gray_Contour.png"));

		base3 = ImageIO.read(new File("TP56/TEST_IMAGES/eae356a5998cf67ac2fb6cae536cacc4.jpg"));

		red = ImageIO.read(new File("TP56/TEST_IMAGES/red_256.png"));
		green = ImageIO.read(new File("TP56/TEST_IMAGES/green_256.png"));
		blue = ImageIO.read(new File("TP56/TEST_IMAGES/blue_256.png"));
		black = ImageIO.read(new File("TP56/TEST_IMAGES/black_256.png"));
		white = ImageIO.read(new File("TP56/TEST_IMAGES/white_256.png"));
		grey = ImageIO.read(new File("TP56/TEST_IMAGES/grey_256.png"));
		white2 = ImageIO.read(new File("TP56/TEST_IMAGES/white_254.png"));

		base4 = ImageIO.read(new File("TP56/TEST_IMAGES/HOT Balloon Trip_Ultra HD.jpg"));
	}

	private IImageFilteringEngine createEngine(int i){
		if(i==1)
			return new SingleThreadedImageFilteringEngine();
		else
			return new MultiThreadedImageFilteringEngine(i);		
	}

	public boolean compareImages(BufferedImage imgA, BufferedImage imgB) {
		// The images must be the same size.
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			int width = imgA.getWidth();
			int height = imgA.getHeight();

			// Loop over every pixel.
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					// Compare the pixels for equality.
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {

						return false;
					}
				}
			}
		} else {
			return false;
		}

		return true;
	}  

	private synchronized boolean testGreyLevelFilter(int nbWorker, BufferedImage imgIn, BufferedImage imgExpected){

		IImageFilteringEngine im = createEngine(nbWorker);
		long t = System.currentTimeMillis();
		im.setImg(imgIn);
		im.applyFilter(new GreyLevelFilter());
		t = System.currentTimeMillis()-t;
		System.out.println("nbWorkers = "+nbWorker+" "+t+"ms");
		listData.add((int)t);
		listLabels.add((int)nbWorker);
		return compareImages(im.getImg(),imgExpected);
	}

	private synchronized boolean testGreyLevelContourFilter(int nbWorker, BufferedImage imgIn, BufferedImage imgExpected){

		IImageFilteringEngine im = createEngine(nbWorker);
		long t = System.currentTimeMillis();
		im.setImg(imgIn);
		im.applyFilter(new GreyLevelFilter());
		im.applyFilter(new BasicContourExtractorFilter());
		t = System.currentTimeMillis()-t;
		System.out.println("nbWorkers = "+nbWorker+" "+t+"ms");
		listData.add((int)t);
		listLabels.add((int)nbWorker);
		return compareImages(im.getImg(),imgExpected);
	}

	private synchronized void testPerfGreyLevelContourFilter(int nbWorker, BufferedImage imgIn){
		IImageFilteringEngine im = createEngine(nbWorker);
		long t = System.currentTimeMillis();
		im.setImg(imgIn);
		im.applyFilter(new GreyLevelFilter());
		im.applyFilter(new BasicContourExtractorFilter());
		t = System.currentTimeMillis()-t;
		listData.add((int)t);
		listLabels.add((int)nbWorker);
		System.out.println("nbWorkers = "+nbWorker+" "+t+"ms");
	}
	
	@Test
	public void testGreyFilter() throws Exception {
		System.out.println("=========== Image 256 x 256 ==========");
		System.out.println("=========== Grey Filter    ==========");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter "+i+"  Thread",testGreyLevelFilter(i,red,grey));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter "+i+"  Thread",testGreyLevelFilter(i,green,grey));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter "+i+"  Thread",testGreyLevelFilter(i,blue,grey));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter "+i+"  Thread",testGreyLevelFilter(i,black,black));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter "+i+"  Thread",testGreyLevelFilter(i,white,white));
		}
		System.out.println();
		//writeJSSerie("Image 1");
		listData.clear();
		listLabels.clear();
	} 

	@Test
	public void testGreyFilter2() throws Exception {
		System.out.println("=========== Image 800 x 450 ==========");
		System.out.println("=========== Filtre Grey     ==========");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter "+i+"  Thread",testGreyLevelFilter(i,base1,base1Grey));
		}
		System.out.println();
		writeJSSerie("Image 800x450 GF");
	} 

	@Test
	public void testGreyFilter3() throws Exception {
		System.out.println("=========== Image 640 x 640 ==========");
		System.out.println("=========== Filtre Grey     ==========");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter "+i+"  Thread",testGreyLevelFilter(i,base2,base2Grey));
		}
		System.out.println();
		writeJSSerie("Image 640x640 GF");
	}
	
	@Test
	public void testGreyContourFilter() throws Exception {
		System.out.println("=========== Image 256 x 256 ==========");
		System.out.println("=========== Filtre Grey + Contour    ==========");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Contour Filter "+i+"  Thread",testGreyLevelContourFilter(i,red,white2));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Contour Filter "+i+"  Thread",testGreyLevelContourFilter(i,green,white2));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Contour Filter "+i+"  Thread",testGreyLevelContourFilter(i,blue,white2));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Contour Filter "+i+"  Thread",testGreyLevelContourFilter(i,black,white2));
		}
		//writeJSSerie("Image 1");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Contour Filter "+i+"  Thread",testGreyLevelContourFilter(i,white,white2));
		}
		System.out.println();
		//writeJSSerie("Image 1");
		listData.clear();
		listLabels.clear();
	}

	@Test
	public void testGreyContourFilter2() throws Exception {
		System.out.println("=========== Image 800 x 450 ==========");
		System.out.println("=========== Grey + Contour Filter  ==========");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter Contour "+i+"  Thread",testGreyLevelContourFilter(i,base1,base1GreyContour));
		}
		System.out.println();
		writeJSSerie("Image 800x450 GCF");
	}

	@Test
	public void testGreyContourFilter3() throws Exception {
		System.out.println("=========== Image 640 x 640 ==========");
		System.out.println("=========== Grey + Contour Filter  ==========");
		for (int i = 1; i <= k; i*=2) {
			assertTrue("Error : Grey Filter Contour "+i+"  Thread",testGreyLevelContourFilter(i,base2,base2GreyContour));
		}
		System.out.println();
		writeJSSerie("Image 640x640 GCF");
	}

//	@Test
//	public void testImage8k() throws Exception {
//		System.out.println("=========== Image 7680 x 4320 ==========");
//		System.out.println("=========== Grey + Contour Filter  ==========");
//		for (int i = 1; i <= k; i*=2) {
//			testPerfGreyLevelContourFilter(i,base3);
//		}
//		System.out.println();
//		writeJSSerie("Image 8k GCF");
//	}
	
	@Test
	public void testImage4k() throws Exception {
		System.out.println("=========== Image 3840 x 2160 ==========");
		System.out.println("=========== Grey + Contour Filter  ==========");
		for (int i = 1; i <= k; i*=2) {
			testPerfGreyLevelContourFilter(i,base4);
		}
		System.out.println();
		writeJSSerie("Image 4k GCF");
	}
	
	@AfterClass
	public static void writeJS(){
		try{
		    PrintWriter writer = new PrintWriter("TP56/results/series.js", "UTF-8");
		    writer.println("function loadSeries(){");
		    writer.println(stb.toString());
		    writer.println("}");
		    writer.close();
		} catch (IOException e) {		}
	}
	
	private void writeJSSerie(String s){
		stb.append("var bar"+(++serieCounter)+" = new Tee.Bar();");
		stb.append("bar"+serieCounter+".title = '"+ s +"';");
		stb.append("bar"+serieCounter+".data.values = "+listData.toString()+";");
		stb.append("bar"+serieCounter+".data.labels =  "+listLabels.toString()+";");
		stb.append("Chart1.addSeries(bar"+serieCounter+");");
		stb.append(System.lineSeparator());
		listData.clear();
		listLabels.clear();
	}
	
}
