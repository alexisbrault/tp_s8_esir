package ex2;
import java.util.concurrent.Exchanger;

public class Player implements Runnable {
		private String name;
		private String sound;
		private Exchanger<String> exchanger;
		private int iteration = 0;

		public Player(String name, String sound, Exchanger<String> exchanger) {
			this.exchanger = exchanger;
			this.name = name;
			this.sound = sound;
		}

		public synchronized void run() {
			String currentSound = sound;
			try {
				while (currentSound != null) {
					play(currentSound);
					sleep();
					wait((int)(Math.random() * 5000));
					ready();
					currentSound = exchanger.exchange(currentSound);
					completedExchange();
					++iteration;
				}
			} catch (InterruptedException ex) { 
				ex.printStackTrace();
			}
		}

		private  synchronized void sleep(){
			System.err.println("Iteration "+iteration+" :"+name+" is going to sleep");
		}

		private  synchronized void ready(){
			System.err.println("Iteration "+iteration+" :"+name+" is ready to exchange");
		}

		private  synchronized void completedExchange(){
			System.err.println("Iteration "+iteration+" :"+name+" exchange completed");
		}

		private  synchronized void play(String sound){
			System.err.println("Iteration "+iteration+" :"+name+" has "+sound);
		}
}
