package ex2;
import java.util.concurrent.Exchanger;

public class PingPongGame {

	public static void main(String args[]){
		Exchanger<String> exchanger = new Exchanger<String>();

		Thread Alice = new Thread(new Player("Alice","Ping", exchanger));
		Thread Bob = new Thread(new Player("Bob","Pong", exchanger));

		Alice.start();
		Bob.start();
	}
}