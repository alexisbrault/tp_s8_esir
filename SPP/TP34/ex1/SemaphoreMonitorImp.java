package ex1;

public class SemaphoreMonitorImp implements SemaphoreInterface {

	private int nbWaitingThread = 0;
	private int token = 0;
	
	public SemaphoreMonitorImp() {

	}

	@Override
	public synchronized void up() {
		token++;
		//On r�veille le thread
		notify();
	}

	@Override
	public synchronized void down() {
		
		try {

			while(token <= 0){
				nbWaitingThread++;
				//Assure le nombre de thread, conflit entre down() et releaseAll()
				int nbThreadWatingTemp = nbWaitingThread;
				//On attend
				wait();
				if(nbThreadWatingTemp>nbWaitingThread){
					return;
				}
				nbWaitingThread--;
			}
				
			token--;
			

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized int releaseAll() {
		int ret = nbWaitingThread;		
		notifyAll();
		//On assure au compteur qu'aucun Thread n'attend
		nbWaitingThread = 0;
		return ret;
	}

}
