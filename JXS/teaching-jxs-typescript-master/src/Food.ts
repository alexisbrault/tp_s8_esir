export class Food{

	public x : number;
	public y : number;

	constructor(public gridWidth : number,public gridHeight : number){

	}

	relocate(){
		this.x = Math.floor(Math.random() * (this.gridWidth-1));
		this.y = Math.floor(Math.random() * (this.gridHeight-1));
	}


  	draw(canvas : CanvasRenderingContext2D, gridSize : number) {
	    canvas.fillStyle = 'yellow';
	    canvas.strokeStyle = 'black';
	    canvas.rect((gridSize * this.x), (gridSize * this.y), gridSize, gridSize)
	    canvas.fillRect((gridSize * this.x), (gridSize * this.y), gridSize, gridSize);
	    //canvas.stroke();
  	}
}