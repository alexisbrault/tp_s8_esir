import {Food} from "./Food"

export class SuperFood extends Food{

	n : number;

	constructor(public gridWidth : number,public gridHeight : number, public time : number){
		super(gridWidth,gridHeight);
		this.n = 0;
	}

	update(){
		this.n++;
		console.log(this.n);
		if(this.n>this.time){
			this.relocate();
			this.n = 0;
		}
	}


  	draw(canvas : CanvasRenderingContext2D, gridSize : number) {
	    canvas.fillStyle = 'green';
	    canvas.rect((gridSize * this.x), (gridSize * this.y), gridSize, gridSize)
	    canvas.fillRect((gridSize * this.x), (gridSize * this.y), gridSize, gridSize);
	    //canvas.stroke();
  	}
}