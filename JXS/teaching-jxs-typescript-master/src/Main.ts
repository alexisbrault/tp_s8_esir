import {Game} from "./Game"

const canvas = <HTMLCanvasElement> document.getElementById("snakeGame");
const score = <HTMLCanvasElement> document.getElementById("score");
const speed = 10;
const game = new Game(canvas, speed,score);
game.start();
game.draw();