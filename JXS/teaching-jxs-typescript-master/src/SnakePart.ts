export class SnakePart{

	constructor(public x : number, public y : number){

	}



  draw(canvas : CanvasRenderingContext2D, gridSize : number) {
    canvas.fillStyle = 'red';
    canvas.strokeStyle = 'black';
    canvas.rect((gridSize * this.x), (gridSize * this.y), gridSize, gridSize)
    canvas.fillRect((gridSize * this.x), (gridSize * this.y), gridSize, gridSize);
    //canvas.stroke();
  }
}