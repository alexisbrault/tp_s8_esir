import {SnakePart} from "./SnakePart"
import {SnakeDirection} from "./SnakeDirection"

export class Snake{
	private parts : Array<SnakePart>;

	constructor(size:number, public beginAtX:number, public beginAtY:number, public dir : SnakeDirection){
		this.parts = new Array<SnakePart>();

	    for (var i = 0; i < size; i++) {
	      var part = new SnakePart(beginAtX - i, beginAtY); 
	      this.parts.push(part);
	    }
	}

	draw(canvas : CanvasRenderingContext2D, gridSize : number) {
    	this.parts.forEach(function(elem) {elem.draw(canvas, gridSize);});
  	}

  	updateGame(gridWidth : number,gridHeight : number){
  		var x = this.parts[0].x;
        var y = this.parts[0].y;

        for(var i=1; i<this.parts.length; i++){
            console.log("X["+i+"] = "+this.parts[i].x);
            console.log("Y["+i+"] = "+this.parts[i].y);
            if(x == this.parts[i].x && y == this.parts[i].y){
                return true;
            }
        }

        if(x==-1 || y==-1 || x==gridHeight || y==gridWidth){
            return true;
        }

        return false;

  	}

  	grow(){
  		var part = null;
  		switch(this.dir) {
            case SnakeDirection.LEFT:
            	part = new SnakePart(this.parts[this.parts.length-1].x+1, this.parts[this.parts.length-1].y); 
            break;

            case SnakeDirection.RIGHT:
            	part = new SnakePart(this.parts[this.parts.length-1].x-1, this.parts[this.parts.length-1].y);
            break;

            case SnakeDirection.UP:
				part = new SnakePart(this.parts[this.parts.length-1].x, this.parts[this.parts.length-1].y+1);
            break;

            case SnakeDirection.DOWN:
               	part = new SnakePart(this.parts[this.parts.length-1].x, this.parts[this.parts.length-1].y-1);
            break;
        }
  		
	    this.parts.push(part);
  	}

  	move(){
  		for (var i = this.parts.length -1; i >= 0 ; i--) {
	      switch(this.dir) {
            case SnakeDirection.LEFT:
            	if(i==0)
                	this.parts[i].x=this.parts[i].x-1;
            break;

            case SnakeDirection.RIGHT:
            	if(i==0)
                	this.parts[i].x=this.parts[i].x+1;
            break;

            case SnakeDirection.UP:
				if(i==0)
                	this.parts[i].y=this.parts[i].y-1;
            break;

            case SnakeDirection.DOWN:
                if(i==0)
                	this.parts[i].y=this.parts[i].y+1;
            break;
    		}
    		if(i!=0){
    			this.parts[i].x=this.parts[i-1].x;
    			this.parts[i].y=this.parts[i-1].y;
    		}
	    }  		
  	}
}