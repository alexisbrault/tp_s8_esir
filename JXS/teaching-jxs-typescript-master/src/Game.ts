import {SnakeDirection} from "./SnakeDirection"
import {Snake} from "./Snake"
import {Food} from "./Food"
import {SuperFood} from "./SuperFood"

export class Game {

    canvasContext : CanvasRenderingContext2D;
    gridWidth : number;
    gridHeight : number;
    snake : Snake;
    food : Food;
    superFood : SuperFood;
    points : number;

    constructor(public canvas : HTMLCanvasElement, public speed : number, public score : HTMLCanvasElement, public gridSize : number = 5) {
        this.gridWidth = canvas.width / gridSize;
        this.gridHeight = canvas.height / gridSize;
        this.canvasContext = canvas.getContext("2d");

        // TODO : listen to user interaction
        addEventListener("keyup",(evt) => this.keyboardUsed(evt));

        this.points = 0;
    }

    /**
     * Start game
     */
    start() {
        // TODO : initialize game
        this.snake = new Snake(3, this.gridWidth / 2, this.gridHeight / 2, SnakeDirection.RIGHT);
        this.food = new Food(this.gridWidth,this.gridHeight);
        this.food.relocate();

        this.superFood = new SuperFood(this.gridWidth,this.gridHeight,20);
        this.superFood.relocate();

        this.animate(); // Start animation
    }

    draw() {
        this.canvasContext.fillStyle = '#CCCCCC';
        this.canvasContext.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.snake.draw(this.canvasContext, this.gridSize);
        this.food.draw(this.canvasContext, this.gridSize);
        this.superFood.draw(this.canvasContext, this.gridSize);
    }

    animate() {
        let fps = this.speed;
        let now;
        let then = Date.now();
        let interval = 1000/fps;
        let delta;

        let animationLoop = (function () {
            if (!this.isGameOver) {
                requestAnimationFrame(animationLoop);
            }

            now = Date.now();
            delta = now - then;

            if (delta > interval) {
                then = now - (delta % interval);
                this.update();
            }

        }).bind(this);

        animationLoop();
    }

    /**
     * Update status of game and view
     */
    update() {
        // TODO
        console.log("update");
        this.snake.move();
        this.draw();
        this.updateGame();
        this.superFood.update();
    }

    updateGame(){
        this.isGameOver = this.snake.updateGame(this.gridHeight,this.gridWidth);
        if(this.snake.parts[0].x==this.food.x && this.snake.parts[0].y==this.food.y){
            this.snake.grow();
            this.food.relocate();
            this.score.innerHTML = (++this.points);
        }
        if(this.snake.parts[0].x==this.superFood.x && this.snake.parts[0].y==this.superFood.y){
            this.snake.grow();
            this.superFood.relocate();
            this.points= this.points+5;
            this.score.innerHTML = this.points;
        }
    }

    /*
    * Function launch by the keyUp listener 
    */
    keyboardUsed(evt : KeyboardEvent) {
        switch(evt.keyCode) {

            case 37: if (this.snake.dir != SnakeDirection.RIGHT)
                this.snake.dir = SnakeDirection.LEFT;
            break;

            case 38: if (this.snake.dir != SnakeDirection.DOWN)
                this.snake.dir = SnakeDirection.UP;
            break;

            case 39: if (this.snake.dir != SnakeDirection.LEFT)
                this.snake.dir = SnakeDirection.RIGHT;
            break;

            case 40: if (this.snake.dir != SnakeDirection.UP)
                this.snake.dir = SnakeDirection.DOWN;
            break;
    }
    }

}