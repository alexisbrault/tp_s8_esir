import { Component, OnInit } from '@angular/core';
import {Pokemon} from '../pokemon';
import {PokemonNamePipe} from '../pokemon-name.pipe'

@Component({
  selector: 'pokesir-poke-input',
  templateUrl: './poke-input.component.html',
  styleUrls: ['./poke-input.component.css']
})
export class PokeInputComponent implements OnInit {
  public listPokemon : Array<Pokemon>;
  public choixDresseur : number;
  public filtre : string;

  constructor() {
      this.listPokemon = new Array<Pokemon>();
      var pok1 = new Pokemon (25, "Pikachu");
      var pok2 = new Pokemon (24, "Arbok");
      var pok3 = new Pokemon (1, "Bulbizarre");
      var pok4 = new Pokemon (4, "Salameche");
      var pok5 = new Pokemon (7, "Carapuce");
      this.listPokemon.push(pok1);
      this.listPokemon.push(pok2);
      this.listPokemon.push(pok3);
      this.listPokemon.push(pok4);
      this.listPokemon.push(pok5);
  }

  go(){
    console.log(this.choixDresseur);
  }

  ngOnInit() {
  }

}
