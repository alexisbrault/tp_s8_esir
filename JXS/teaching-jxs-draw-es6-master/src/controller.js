
const editingMode = { rect: 0, line: 1 };

class Pencil {
	constructor(ctx, drawing, canvas) {
        this.currEditingMode = editingMode.line;
        this.currLineWidth = 5;
        this.currColour = '#000000';
        this.currentShape = null;
        new DnD(canvas, this);

        // Liez ici les widgets à la classe pour modifier les attributs présents ci-dessus.
        document.getElementById('butRect').addEventListener('click', this.setEditingModeRect.bind(this), false);
		document.getElementById('butLine').addEventListener('click', this.setEditingModeLine.bind(this), false);
		document.getElementById('spinnerWidth').addEventListener('change', this.setThickness.bind(this), false);
		document.getElementById('colour').addEventListener('input', this.setColor.bind(this), false);

		this.drawing = drawing;
		this.ctx = ctx;
		this.canvas = canvas;
    }


	// Implémentez ici les 3 fonctions onInteractionStart, onInteractionUpdate et onInteractionEnd
	setEditingModeRect(){
		this.currEditingMode = editingMode.rect;
	}

	setEditingModeLine(){
		this.currEditingMode = editingMode.line;
	}

	setThickness(){
		this.currLineWidth = document.getElementById('spinnerWidth').value;
	}

	setColor(){
		this.currColour = document.getElementById('colour').value;
	}

	onInteractionStart(dnd) {
		switch(this.currEditingMode) {
			case editingMode.rect: {
				this.currentShape = new Rectangle(dnd.x1, dnd.y1, 10, 10, this.currLineWidth, this.currColour);
				break;
			}
			case editingMode.line: {
				this.currentShape = new Line(dnd.x1, dnd.y1, dnd.x2, dnd.y2, this.currLineWidth, this.currColour);
				break;
			}
		}
		this.drawing.paint(ctx, canvas);
	}

	onInteractionUpdate(dnd) {
		switch(this.currEditingMode) {
			case editingMode.line: {
				this.currentShape.endAt.x = dnd.x2;
				this.currentShape.endAt.y = dnd.y2;
				break;
			}
			case editingMode.rect: {
				this.currentShape.width = dnd.x2 - dnd.x1;
				this.currentShape.height = dnd.y2 - dnd.y1;
				break;
			}
		}
		this.drawing.paint(ctx, canvas);
		this.currentShape.paint(ctx, canvas);
	}

	onInteractionEnd(dnd) {
		this.drawing.shapeList.push(this.currentShape);
		this.drawing.paint(ctx, canvas);
		updateShapeList(this.currentShape, this.currEditingMode);
	}
}


