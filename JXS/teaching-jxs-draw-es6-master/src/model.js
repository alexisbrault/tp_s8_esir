
// Implémenter ici les 4 classes du modèle.
// N'oubliez pas l'héritage !
class Drawing {
  constructor() {
    this.shapeList = new Array();
  }
}

class Shape {
  constructor(thickness, color) {
    this.thickness = thickness;
    this.color = color;
  }
}

class Rectangle extends Shape {
  constructor(x1, y1, width, height, thickness, color) {
    super(thickness, color);
    this.coord = {
      x: x1,
      y: y1
    }
    this.width = width;
    this.height = height;
  }
}

class Line extends Shape {
  constructor(x1, y1, x2, y2, thickness, color) {
    super(thickness, color);
    this.beginAt = {
      x: x1,
      y: y1
    }
    this.endAt = {
      x: x2,
      y: y2
    }
  }
}