// La création d'un Dnd requière un canvas et un interacteur.
// L'interacteur viendra dans un second temps donc ne vous en souciez pas au départ.
class DnD {
  constructor(canvas, interactor) {
    this.canvas = canvas;
    this.interactor = interactor;

    // Définir ici les attributs de la classe
    this.x1 = 0;
    this.y1 = 0;

    this.x2 = 0;
    this.y2 = 0;

    this.isMousePressed = false; //Indique si le bouton de la souris a été appuyé
    // Associer les évènements du canvas aux fonctions ci-dessous.
    canvas.addEventListener('mousedown', this.mouseDown.bind(this), false);
    canvas.addEventListener('mousemove', this.mouseMove.bind(this), false);
    canvas.addEventListener('mouseup', this.mouseUp.bind(this), false);

  }

  // Developper les 3 fonctions gérant les événements
  mouseDown(evt) {
    this.x1 = getMousePosition(canvas, evt).x;
    this.y1 = getMousePosition(canvas, evt).y;
    this.isMousePressed = true;
    this.interactor.onInteractionStart(this);
    console.log("mouseDown (", this.x1, ":", this.y1,")");
  }

  mouseMove(evt) {
    if (this.isMousePressed) {
      this.x2 = getMousePosition(canvas, evt).x;
      this.y2 = getMousePosition(canvas, evt).y;
      
      console.log("mouseMove (", getMousePosition(canvas, evt).x, ":", getMousePosition(canvas, evt).y,")");
      this.interactor.onInteractionUpdate(this);
    }
  }

  mouseUp(evt) {
    if (this.isMousePressed) {
      this.x2 = getMousePosition(canvas, evt).x;
      this.y2 = getMousePosition(canvas, evt).y;
      this.isMousePressed = false;

      console.log("mouseUp (", this.x2, ":", this.y2,")");
      this.interactor.onInteractionEnd(this);
    }
  }

}


// Place le point de l'événement evt relativement à la position du canvas.
function getMousePosition(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}