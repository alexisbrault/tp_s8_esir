
// Implémenter ici les fonctions paint à ajouter dans chacune des classes du modèle.

Drawing.prototype.paint = function(ctx, canvas) {

  ctx.fillStyle = '#F0F0F0'; // set canvas' background color
  ctx.fillRect(0, 0, canvas.width, canvas.height);  // now fill the canvas

  this.shapeList.forEach(function(elt) {elt.paint(ctx)})
}

Shape.prototype.paint = function(ctx) {
  ctx.lineWidth = this.thickness;
  ctx.strokeStyle = this.color;
}

Line.prototype.paint = function(ctx) {
  Shape.prototype.paint.call(this, ctx);

  ctx.beginPath();
  ctx.moveTo(this.beginAt.x, this.beginAt.y);
  ctx.lineTo(this.endAt.x, this.endAt.y);
  ctx.stroke();
}

Rectangle.prototype.paint = function(ctx) {
  Shape.prototype.paint.call(this, ctx);

  ctx.beginPath();
  ctx.rect(this.coord.x, this.coord.y, this.width, this.height);
  ctx.stroke();
}

function updateShapeList(shape, editMode) {
  var div = document.getElementById('shapeList');

  var newSpan = document.createElement("span");
  newSpan.innerText = shape.toString();

  var i = document.createElement("i");
  i.className += "fa fa-times";
  i.addEventListener('click', function(){removeShape(this);}, false);

  newSpan.appendChild(i);
  div.appendChild(newSpan);
}

function removeShape(a){
	var item = a.parentElement;
	var list = item.parentElement.children;
	var index = Array.prototype.indexOf.call(list, item);
	drawing.shapeList.splice(index, 1);
	drawing.paint(ctx, canvas);
	item.remove(); 
}

Line.prototype.toString = function(ctx) {
   return 'Ligne (' + this.beginAt.x + ',' + this.beginAt.y + ',' + this.endAt.x + ',' + this.endAt.y + ')';
}

Rectangle.prototype.toString = function(ctx) {
  return 'Rectangle (' + this.coord.x + ',' + this.coord.y + ',' + this.width + ',' + this.height + ')';
}